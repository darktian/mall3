package com.mall.order;

import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;

/**
 * @author liangJiapeng
 * @create 2021-07-22 14:31
 * describe:
 */
public interface OrderDeleteService {

    DeleteOrderResponse deleteOrder(DeleteOrderRequest request);
}
