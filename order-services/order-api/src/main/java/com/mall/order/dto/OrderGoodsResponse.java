package com.mall.order.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderGoodsResponse extends AbstractResponse
{
    private OrderGoods orderGoods;
}
