package com.mall.order;


import com.mall.order.dto.*;


/**
 *  ciggar
 * create-date: 2019/7/30-上午10:01
 */
public interface OrderQueryService {

 OrderListResponse queryOrderList(OrderListRequest request);


    CancelOrderResponse cancelOrder(CancelOrderRequest cancelOrderRequest);


 OrderGoodsResponse queryOrderDetail(OrderGoodsRequest request);

    void updatePayCount(String orderId);
}
