package com.mall.order.dto;


import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderGoods extends AbstractResponse {

    private String userName;
    /*
    订单总费用
     */
    private BigDecimal orderTotal;
    private String userId;
    private List<OrderItemResponse> goodsList;
    private String tel;
    private String streetName;
    private Integer orderStatus;
}
