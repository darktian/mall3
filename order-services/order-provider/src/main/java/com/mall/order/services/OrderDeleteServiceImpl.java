package com.mall.order.services;

import com.mall.order.OrderDeleteService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;
import com.mall.order.dto.OrderItemDto;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

/**
 * ciggar
 * create-date: 2019/7/30-上午10:05
 */
@Slf4j
@Component
@Service
public class OrderDeleteServiceImpl implements OrderDeleteService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderItemMapper orderItemMapper;



    @Override
    public DeleteOrderResponse deleteOrder(DeleteOrderRequest request) {
        //参数校验
        request.requestCheck();
        //new VO
        DeleteOrderResponse response = new DeleteOrderResponse();
        try {

            //删除订单表
            int delete = orderMapper.deleteByPrimaryKey(request.getOrderId());
            if (delete != 0) {
                response.setCode(OrderRetCode.SUCCESS.getCode());
                response.setMsg(OrderRetCode.SUCCESS.getMessage());

                //删订单商品表
                OrderItem orderItem = new OrderItem();
                orderItem.setOrderId(request.getOrderId());
                Example example = new Example(OrderItem.class);
                example.createCriteria()
                        .andEqualTo("orderId",request.getOrderId());
                int deleteByExample = orderItemMapper.deleteByExample(example);


            } else {
                response.setCode(OrderRetCode.SYSTEM_ERROR.getCode());
                response.setMsg(OrderRetCode.SYSTEM_ERROR.getMessage());
            }

        } catch (Exception e) {
            log.info("OrderDeleteServiceImpl#deleteOrder" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }
}
