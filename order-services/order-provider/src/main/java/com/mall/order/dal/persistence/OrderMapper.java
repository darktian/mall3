package com.mall.order.dal.persistence;

import com.mall.commons.tool.tkmapper.TkMapper;
import com.mall.order.dal.entitys.Order;

import org.apache.ibatis.annotations.Param;

import java.util.Date;

import com.mall.order.dto.OrderDetailInfo;
import com.mall.order.dto.OrderGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface OrderMapper extends TkMapper<Order> {
    Long countAll();


    int updateCancelOrder(@Param("orderId") String orderId, @Param("status") Integer status, @Param("updateTime") Date updateTime);

    List<OrderDetailInfo> getOrderDetailByUserId(@Param("id") Long id, @Param("size") Integer size, @Param("page") Integer page);

    OrderGoods getOrderGoodsResponse(@Param("orderId") String orderId);

    void updateByOrder(String orderId);
}