package com.mall.order.mq.order;

import com.mall.order.config.MQConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/23/19:00
 */

@Component
@Slf4j
public class DelayOrderProducer {

    private DefaultMQProducer producer;

    @Autowired
    MQConfiguration mqConfiguration;

    @PostConstruct
    public void init() {
        producer = new DefaultMQProducer(mqConfiguration.getProducerGroup());
        producer.setNamesrvAddr(mqConfiguration.getNameServerAdd());
        try {
            producer.start();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }

    public boolean sendOrderDelayMessage(String orderId) {
        try {
            byte[] bytes = orderId.getBytes(mqConfiguration.getCharset());
            Message message = new Message(mqConfiguration.getTopic(), bytes);
            message.setDelayTimeLevel(mqConfiguration.getLevel());
            SendResult result = producer.send(message);
            if (SendStatus.SEND_OK.equals(result.getSendStatus())) {
                log.debug(result.getMsgId());
                return true;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (MQClientException e) {
            e.printStackTrace();
        } catch (MQBrokerException e) {
            e.printStackTrace();
        }
        return false;
    }
}
