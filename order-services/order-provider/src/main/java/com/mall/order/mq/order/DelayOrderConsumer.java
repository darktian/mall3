package com.mall.order.mq.order;

import com.mall.order.OrderQueryService;
import com.mall.order.config.MQConfiguration;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import org.apache.rocketmq.client.consumer.DefaultMQPullConsumer;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/23/19:33
 */

@Component
public class DelayOrderConsumer {

    private DefaultMQPushConsumer consumer;

    @Autowired
    MQConfiguration mqConfiguration;

    @Autowired
    OrderQueryService queryService;

    @Autowired
    OrderMapper orderMapper;
    @PostConstruct
    public void init() {
        consumer = new DefaultMQPushConsumer(mqConfiguration.getConsumerGroup());
        consumer.setNamesrvAddr(mqConfiguration.getNameServerAdd());
        try {
            consumer.subscribe(mqConfiguration.getTopic(), "*");
            consumer.setMessageListener(new MessageListenerConcurrently() {
                @Override
                public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                    for (MessageExt messageExt : list) {
                        byte[] data = messageExt.getBody();
                        try {
                            String orderId = new String(data, 0, data.length, mqConfiguration.getCharset());
                            CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
                            cancelOrderRequest.setOrderId(orderId);

                            Order order = orderMapper.selectByPrimaryKey(orderId);
                            if(order.getStatus().equals(0)){
                                CancelOrderResponse cancelOrderResponse = queryService.cancelOrder(cancelOrderRequest);
                                if (OrderRetCode.DB_EXCEPTION.getCode().equals(cancelOrderResponse.getCode())) {
                                    return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                                }
                            }

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
                        }
                    }
                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                }
            });
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        try {
            consumer.start();
        } catch (MQClientException e) {
            e.printStackTrace();
        }

    }
}
