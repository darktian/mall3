package com.mall.order.biz.handler;

import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.mq.order.DelayOrderProducer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;

/**
 * @Description: 利用mq发送延迟取消订单消息
 * @Author： ciggar
 * @Date: 2019-09-17 23:14
 **/
@Component
@Slf4j
public class SendMessageHandler extends AbstractTransHandler {

    @Autowired
    DelayOrderProducer delayOrderProducer;

    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        CreateOrderContext createOrderContext = (CreateOrderContext) context;

        int count = 3;

        boolean re = delayOrderProducer.sendOrderDelayMessage(createOrderContext.getOrderId());
        while ( count<=0 || !re) {
            delayOrderProducer.sendOrderDelayMessage(createOrderContext.getOrderId());
            count--;
        }
        return true;
    }
}