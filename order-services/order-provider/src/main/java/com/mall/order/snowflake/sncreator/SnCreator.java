package com.mall.order.snowflake.sncreator;

/**
 *
 * @author kingapex
 * @version 1.0
 * @since 7.1.0
 * 2019-11-22
 */
public interface SnCreator {

    /**
     * 生成唯一数字
     * @return
     */
    String create(int subId);
}
