package com.mall.order.services;

import com.mall.order.OrderCancelService;
import com.mall.order.OrderDeleteService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;
import com.mall.shopping.constants.ShoppingRetCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

/**
 * ciggar
 * create-date: 2019/7/30-上午10:05
 */
@Slf4j
@Component
@Service
public class OrderCancelServiceImpl implements OrderCancelService {

    @Autowired
    OrderMapper orderMapper;

//    @Override
//    public CancelOrderResponse cancelOrder(CancelOrderRequest request) {
//        //参数校验
//        request.requestCheck();
//        //new vo
//        CancelOrderResponse response = new CancelOrderResponse();
//        try {
//            //order表
//            Order order = new Order();
//            order.setStatus(5);//取消订单就是订单关闭状态,设为5
//            order.setUpdateTime(new Date());//订单更新时间
//            order.setCloseTime(new Date());//交易关闭时间
//
//            Example example1 = new Example(Order.class);
//            example1.createCriteria()
//                    .andEqualTo("orderId",request.getOrderId());
//            int OrderaffectiveRows= orderMapper.updateByExampleSelective(order, example1);
//
//
//            //order_item表
//            OrderItem orderItem = new OrderItem();
//            orderItem.setOrderId(request.getOrderId());
//
//
//            Example example2 = new Example(OrderItem.class);
//            example2.createCriteria()
//                    .andEqualTo("orderId",request.getOrderId());
//            int OrderItemaffectiveRows= orderMapper.updateByExampleSelective(order, example2);
//
//
//            //mapper
//
//
//
//
//
//
//        } catch (Exception e) {
//            log.info("OrderCancelServiceImpl#cancelOrder" + e);
//            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
//            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
//        }
//        return response;
//
//    }


}
