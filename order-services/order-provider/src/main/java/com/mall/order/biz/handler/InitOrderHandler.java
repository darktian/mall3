package com.mall.order.biz.handler;

import com.google.errorprone.annotations.Var;
import com.mall.commons.tool.exception.BizException;
import com.mall.commons.tool.utils.NumberUtils;
import com.mall.order.biz.callback.SendEmailCallback;
import com.mall.order.biz.callback.TransCallback;
import com.mall.order.biz.context.CreateOrderContext;
import com.mall.order.biz.context.TransHandlerContext;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.constants.OrderConstants;
import com.mall.order.dal.entitys.Order;
import com.mall.order.dal.entitys.OrderItem;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dto.CartProductDto;
import com.mall.order.snowflake.sncreator.SnCreator;
import com.mall.order.utils.GlobalIdGeneratorUtil;
import com.mall.user.IUserLoginService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Member;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * ciggar
 * create-date: 2019/8/1-下午5:01
 * 初始化订单 生成订单
 */

@Slf4j
@Component
public class InitOrderHandler extends AbstractTransHandler {
    //发号器
    @Autowired
    SnCreator snCreator;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderItemMapper orderItemMapper;

    @Reference(timeout = 3000, check = false)
    IUserLoginService iUserLoginService;

    @Override
    public boolean isAsync() {
        return false;
    }

    @Override
    public boolean handle(TransHandlerContext context) {
        CreateOrderContext createOrderContext = (CreateOrderContext) context;
        try {
            //插入订单
            Order order = new Order();
            String orderId =  snCreator.create(1);
            //发号器
            order.setOrderId(orderId);
            order.setUserId(createOrderContext.getUserId());

            String userName = iUserLoginService.getUserInfoById(createOrderContext.getUserId());

            createOrderContext.setBuyerNickName(userName);
            order.setBuyerNick(userName);
            order.setPayment(createOrderContext.getOrderTotal());
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            order.setStatus(OrderConstants.ORDER_STATUS_INIT);

            int orderEffectedRows = orderMapper.insert(order);

            if (orderEffectedRows < 1) {
                throw new BizException(OrderRetCode.DB_SAVE_EXCEPTION.getCode(), OrderRetCode.DB_SAVE_EXCEPTION.getMessage());
            }
            //插入订单商品关联表
            List<Long> buyProductIdList = new ArrayList<>();

            List<CartProductDto> cartProductDtoList = createOrderContext.getCartProductDtoList();
            for (CartProductDto cartProductDto : cartProductDtoList) {
                OrderItem orderItem = new OrderItem();
                String orderItemId =  snCreator.create(2);
                orderItem.setId(orderItemId);
                orderItem.setItemId(cartProductDto.getProductId());
                orderItem.setOrderId(orderId);
                orderItem.setNum(cartProductDto.getProductNum().intValue());
                orderItem.setPrice(cartProductDto.getSalePrice().doubleValue());
                orderItem.setTitle(cartProductDto.getProductName());
                orderItem.setPicPath(cartProductDto.getProductImg());
                BigDecimal total = cartProductDto.getSalePrice().multiply(new BigDecimal(cartProductDto.getProductNum()));
                orderItem.setTotalFee(total.doubleValue());

                orderItem.setStatus(1);
                buyProductIdList.add(cartProductDto.getProductId());

                int orderItemEffectedRows = orderItemMapper.insert(orderItem);

                if (orderItemEffectedRows < 1) {
                    throw new BizException(OrderRetCode.DB_SAVE_EXCEPTION.getCode(), OrderRetCode.DB_SAVE_EXCEPTION.getMessage());
                }
            }

            createOrderContext.setOrderId(orderId);
            createOrderContext.setBuyProductIds(buyProductIdList);
        } catch (Exception e) {
            throw new BizException(OrderRetCode.DB_SAVE_EXCEPTION.getCode(), OrderRetCode.DB_SAVE_EXCEPTION.getMessage());
        }

        return true;
    }
}

