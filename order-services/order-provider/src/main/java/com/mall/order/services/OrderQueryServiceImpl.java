package com.mall.order.services;



import com.mall.order.OrderQueryService;

import com.mall.order.constant.OrderRetCode;
import com.mall.order.converter.OrderConverter;
import com.mall.order.dal.entitys.*;
import com.mall.order.dal.persistence.OrderItemMapper;
import com.mall.order.dal.persistence.OrderMapper;
import com.mall.order.dal.persistence.OrderShippingMapper;
import com.mall.order.dal.persistence.StockMapper;
import com.mall.order.dto.*;

import com.mall.shopping.constants.ShoppingRetCode;
import lombok.extern.slf4j.Slf4j;

import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tk.mybatis.mapper.entity.Example;


import java.math.BigDecimal;
import java.util.Date;

import java.util.List;

/**
 * ciggar
 * create-date: 2019/7/30-上午10:04
 */
@Slf4j
@Component
@Service
public class OrderQueryServiceImpl implements OrderQueryService {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderItemMapper orderItemMapper;

    @Autowired
    StockMapper stockMapper;

    @Override
    public CancelOrderResponse cancelOrder(CancelOrderRequest cancelOrderRequest) {
        CancelOrderResponse cancelOrderResponse = new CancelOrderResponse();
        try {
            String orderId = cancelOrderRequest.getOrderId();
            orderItemMapper.updateCancelStock(orderId, 2, new Date());

            orderMapper.updateCancelOrder(orderId, 5, new Date());


            List<OrderItem> orderItems = orderItemMapper.queryByOrderId(orderId);

            Stock stock = stockMapper.selectStockForUpdate(orderItems.get(0).getItemId());

            Integer lockCount = stock.getLockCount();
            stock.setLockCount(-lockCount);
            stock.setStockCount(lockCount.longValue());
            stockMapper.updateStock(stock);
            cancelOrderResponse.setCode(OrderRetCode.SUCCESS.getCode());
            cancelOrderResponse.setMsg(OrderRetCode.SUCCESS.getMessage());
        } catch (NumberFormatException e) {
            cancelOrderResponse.setCode(OrderRetCode.DB_EXCEPTION.getCode());
            cancelOrderResponse.setMsg(OrderRetCode.DB_EXCEPTION.getMessage());
        }

      return cancelOrderResponse;
    }

    OrderShippingMapper orderShippingMapper;

   @Autowired
    OrderConverter orderConverter;


    /**
     * 获取全部订单
     * @param request
     * @return
     */
    @Override
    public OrderListResponse queryOrderList(OrderListRequest request) {
        OrderListResponse response = new OrderListResponse();
        try{

            List<OrderDetailInfo> orderDetails
                    = orderMapper.getOrderDetailByUserId(request.getUserId(), request.getSize(), request.getPage());
            for (OrderDetailInfo orderDetail : orderDetails) {
                Example example = new Example(OrderItem.class);
                example.and().andEqualTo("orderId",orderDetail.getOrderId());
                List<OrderItem> orderItems = orderItemMapper.selectByExample(example);

                List<OrderItemDto> orderItemDtos = orderConverter.item2dto(orderItems);
                orderDetail.setOrderItemDto(orderItemDtos);
            }
            Example example = new Example(Order.class);
            example.and().andEqualTo("userId", request.getUserId());
            int total = orderMapper.selectCountByExample(example);
            response.setTotal(Long.valueOf(total));
            response .setDetailInfoList(orderDetails);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        }
        catch(Exception e){
            log.error("OrderQueryServiceImpl.queryOrderList Occur Exception :" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }

    /**
     * 获取订单详情
     * @param request
     * @return
     */

    @Override
    public OrderGoodsResponse queryOrderDetail(OrderGoodsRequest request) {

        OrderGoodsResponse response = new OrderGoodsResponse();
        try{
            request.requestCheck();
            OrderGoods orderGoods = orderMapper.getOrderGoodsResponse(request.getOrderId());
            List<OrderItemResponse> goodsList = orderGoods.getGoodsList();
            orderGoods.setOrderTotal(new BigDecimal(0));
            //计算订单的总价格
            for (OrderItemResponse good : goodsList) {
                BigDecimal add = orderGoods.getOrderTotal().add(good.getPrice());
                orderGoods.setOrderTotal(add);
            }
            response.setOrderGoods(orderGoods);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        }
        catch (Exception e){
            log.error("OrderQueryServiceImpl.queryOrderDetail Occur Exception :" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }

    @Override
    public void updatePayCount(String orderId) {
        // 2. 修改订单状态，为已支付状态
        orderMapper.updateByOrder(orderId);
        // 3. 修改库存，将订单中商品对应的库存，锁定库存清零
        orderItemMapper.updateStockStatus(orderId,3);

        // 4.  修改订单商品条目，对应的库存状态：3 成功扣减库存
        OrderItem orderItem = new OrderItem();
        orderItem.setOrderId(orderId);
        List<OrderItem> items = orderItemMapper.select(orderItem);
        for (OrderItem item : items) {
            Integer count = item.getNum();
            Long itemId = item.getItemId();
            Stock stock = new Stock();
            stock.setItemId(itemId);
            stock.setStockCount(0L);
            stock.setLockCount(-count);
            stockMapper.updateStock(stock);
        }

    }


}
