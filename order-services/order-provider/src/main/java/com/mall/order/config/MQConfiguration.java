package com.mall.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/23/19:04
 */

@Data
@Component
@ConfigurationProperties(prefix = "mq-producer")
public class MQConfiguration {
    private String producerGroup;
    private String consumerGroup;
    private String nameServerAdd;
    private String topic;
    private String charset = "utf-8";
    //默认是
    private Integer level = 3;
}
