package com.mall.commons.result;

/**
 *
 */


public class ResponseDataAndTotal <T> {
    private Long total;
    private T data;

    public ResponseDataAndTotal() {
    }

    public ResponseDataAndTotal(Long total, T data) {
        this.total = total;
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
