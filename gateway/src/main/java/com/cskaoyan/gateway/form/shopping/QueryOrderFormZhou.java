package com.cskaoyan.gateway.form.shopping;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QueryOrderFormZhou {
    private Integer page;
    private Integer size;
    private String sort;
}
