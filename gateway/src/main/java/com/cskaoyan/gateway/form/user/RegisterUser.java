package com.cskaoyan.gateway.form.user;

import lombok.Data;

import javax.validation.constraints.Email;

/*
 *@Author: WUW
 *@Description
 *
 */
@Data
public class RegisterUser {

    private String userName;
    private String userPwd;
    private String captcha;
    @Email
    private String email;
}
