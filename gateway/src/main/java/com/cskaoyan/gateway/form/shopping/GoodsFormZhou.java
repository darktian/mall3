package com.cskaoyan.gateway.form.shopping;

import com.mall.shopping.dto.AllProductRequest;
import com.mall.shopping.dto.AllProductRequestZhou;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/**
 *
 */
@Data
@ApiModel
public class GoodsFormZhou {
    @ApiModelProperty(name = "page", value = "页码")
    private Integer page;
    @ApiModelProperty(name = "size", value = "每页条数")
    private Integer size;
    @ApiModelProperty(name = "sort", value = "是否排序")
    private Integer sort;
    @ApiModelProperty(name = "priceGt", value = "价格最小值")
    private BigDecimal priceGt;
    @ApiModelProperty(name = "priceLte", value = "价格最大值")
    private BigDecimal priceLte;
    @ApiModelProperty(name = "cid", value = "所属分裂")
    private Long cid;

    public static AllProductRequestZhou goodsForm2Request(GoodsFormZhou goodsForm){
        AllProductRequestZhou productRequest = new AllProductRequestZhou();
        productRequest.setCid(goodsForm.getCid());
        productRequest.setPage(goodsForm.getPage());
        productRequest.setSize(goodsForm.getSize());
        productRequest.setSort(goodsForm.getSort());
        productRequest.setPriceGt(goodsForm.getPriceGt());
        productRequest.setPriceLte(goodsForm.getPriceLte());
        return productRequest;
    }

}
