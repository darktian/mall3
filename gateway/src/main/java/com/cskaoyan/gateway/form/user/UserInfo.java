package com.cskaoyan.gateway.form.user;

import lombok.Data;

/*
 *@Author: WUW
 *@Description
 *
 */
@Data
public class UserInfo {
    private Long uid;
    private String file;
    private String username;
}
