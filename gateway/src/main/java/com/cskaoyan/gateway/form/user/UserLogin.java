package com.cskaoyan.gateway.form.user;

import lombok.Data;

/*
 *@Author: WUW
 *@Description
 *
 */
@Data
public class UserLogin {
    private String userName;
    private String userPwd;
    private String captcha;
}
