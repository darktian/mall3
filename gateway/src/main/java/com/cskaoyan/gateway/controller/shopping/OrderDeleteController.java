package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.form.shopping.AddressForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCancelService;
import com.mall.order.OrderDeleteService;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import com.mall.order.dto.DeleteOrderRequest;
import com.mall.order.dto.DeleteOrderResponse;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.DeleteCheckedItemRequest;
import com.mall.shopping.dto.DeleteCheckedItemResposne;
import com.mall.user.IAddressService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.*;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;

/**
 *
 */
@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "OrderDeleteController", description = "删除订单")
public class OrderDeleteController {

    @Reference(timeout = 300000, check = false)
    OrderDeleteService orderDeleteService;

    @Reference(timeout = 300000, check = false)
    OrderCancelService orderCancelService;

    /**
     * 获取当前用户的地址列表
     *
     * @return
     */

    @RequestMapping(value="/order/{orderId}",method = RequestMethod.DELETE)//请求方法是delete
    public ResponseData deleteOrder(@PathVariable("orderId") Long orderId){
        //new 接收对象request
        DeleteOrderRequest request = new DeleteOrderRequest();
        //数据封装到req
        request.setOrderId(orderId.toString());
        //远程服务调用
        DeleteOrderResponse response = orderDeleteService.deleteOrder(request);

        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())) {
            return new ResponseUtil().setData(response.getMsg());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());


    }


//    @PostMapping(value="/cancelOrder")//取消订单
//    public ResponseData cancelOrder(@RequestParam("id") Long orderId){
//        //new 接收对象request
//        CancelOrderRequest request = new CancelOrderRequest();
//        //数据封装到req
//        request.setOrderId(orderId.toString());
//        //远程服务调用
//        CancelOrderResponse response = orderCancelService.cancelOrder(request);
//
//        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())) {
//            return new ResponseUtil().setData(response.getMsg());
//        }
//        return new ResponseUtil().setErrorMsg(response.getMsg());
//    }







}


