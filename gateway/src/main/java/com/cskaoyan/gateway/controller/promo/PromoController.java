package com.cskaoyan.gateway.controller.promo;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.cache.CacheManager;
import com.cskaoyan.gateway.form.sessionkill.CreatePromoOrderInfo;
import com.google.common.util.concurrent.RateLimiter;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.promo.PromoService;
import com.mall.promo.constant.PromoRetCode;
import com.mall.promo.dto.*;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.intercepter.TokenIntercepter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.*;

@RestController
@RequestMapping("/shopping")
@Slf4j
public class PromoController {

    @Reference(check = false)
    PromoService promoService;
    @Autowired
    CacheManager cacheManager;


    // 通过该工具，来实现令牌桶算法，实现限流
    RateLimiter rateLimiter;

    // 线程池
    ExecutorService executorService;

    @PostConstruct
    public void init() {
        // 创建一个每秒产生100个令牌的令牌桶
       rateLimiter = RateLimiter.create(100);

        executorService = Executors.newFixedThreadPool(100);
    }


    @GetMapping("/seckilllist")
    @Anoymous
    public ResponseData getPromotionList(@RequestParam Integer sessionId) {
        PromoInfoRequest promoInfoRequest = new PromoInfoRequest();
        promoInfoRequest.setSessionId(sessionId);

        String yyyyMMdd = DateFormatUtils.format(new Date(), "yyyyMMdd");
        promoInfoRequest.setYyyymmdd(yyyyMMdd);
        PromoInfoResponse promoInfoResponse = promoService.getPromoList(promoInfoRequest);
        if (!promoInfoResponse.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtil<>().setErrorMsg(promoInfoResponse.getMsg());
        }
        return new ResponseUtil<>().setData(promoInfoResponse);

    }


    @PostMapping("/promoProductDetail")
    public ResponseData getPromoItemDetail(@RequestBody PromoProductDetailRequest request) {

        PromoProductDetailResponse response = promoService.getPromoProductDetail(request);
        if (!response.getCode().equals(SysRetCodeConstants.SUCCESS.getCode())) {
            return new ResponseUtil<>().setErrorMsg(response.getMsg());
        }
        return new ResponseUtil<>().setData(response);
    }


    @PostMapping("/seckill")
    public ResponseData seckill(HttpServletRequest request,
                                @RequestBody CreatePromoOrderInfo createPromoOrderInfo) {

        // 总体的请求限流(令牌桶来做)
        // 如果获取令牌成功，继续向下正常执行，需要等待一段时间
        rateLimiter.acquire();

        Long productId = createPromoOrderInfo.getProductId();
        Long psId = createPromoOrderInfo.getPsId();
        // 先判断有没有售罄
        String key =  "stock_not_enough" + psId + "_" + productId;

        /*
              业务限流的工作
         */
        String s = cacheManager.checkCache(key);
        if (s != null && "notEnough".equals(s.trim())) {
            return new ResponseUtil<>().setErrorMsg("商品售罄");
        }


        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject jsonObject = JSON.parseObject(userInfo);
        String username = (String) jsonObject.get("username");
        Integer uid = (Integer) jsonObject.get("uid");
        CreatePromoOrderRequest createPromoOrderRequest = new CreatePromoOrderRequest();

        createPromoOrderRequest.setProductId(productId);

        createPromoOrderRequest.setPsId(psId);
        createPromoOrderRequest.setUserId(uid.longValue());
        createPromoOrderRequest.setUsername(username);

        //增加地址信息
        createPromoOrderRequest.setAddressId(createPromoOrderInfo.getAddressId());
        createPromoOrderRequest.setStreetName(createPromoOrderInfo.getStreetName());
        createPromoOrderRequest.setTel(createPromoOrderInfo.getTel());

        // 对下游服务的调用放入线程池中来执行
        Future<CreatePromoOrderResponse> future = executorService.submit(new Callable<CreatePromoOrderResponse>() {

            @Override
            public CreatePromoOrderResponse call() throws Exception {
                return promoService.createPromoOrder(createPromoOrderRequest);
            }
        });

        try {
            CreatePromoOrderResponse createPromoOrderResponse = future.get();
            if (PromoRetCode.SUCCESS.getCode().equals(createPromoOrderResponse.getCode())) {
                return new ResponseUtil<>().setData(createPromoOrderResponse);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.SYSTEM_ERROR.getMessage());
    }


}
