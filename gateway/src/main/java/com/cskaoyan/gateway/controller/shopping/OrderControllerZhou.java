package com.cskaoyan.gateway.controller.shopping;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.form.shopping.QueryOrderFormZhou;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseDataAndTotal;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderQueryService;
import com.mall.order.dto.*;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Example;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.zookeeper.data.Id;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
/**
 *
 */
@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "OrderController", description = "获取订单")
public class OrderControllerZhou {


    @Reference(retries = 0,timeout = 100000, check = false)
    OrderQueryService orderQueryService;

    @GetMapping("/order")
    @ApiOperation("获取用户所有的订单")
    public ResponseData getOrdersByUserId(QueryOrderFormZhou queryOrder, HttpServletRequest servletRequest) {
        OrderListRequest orderListRequest = new OrderListRequest();
        orderListRequest.setPage(queryOrder.getPage());
        orderListRequest.setSize(queryOrder.getSize());
        orderListRequest.setSort(queryOrder.getSort());
        String userInfo = (String) servletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        long uid = Long.parseLong(String.valueOf(object.get("uid")));
        orderListRequest.setUserId(uid);
        OrderListResponse response = orderQueryService.queryOrderList(orderListRequest);
        if(ShoppingRetCode.SUCCESS.getCode().equals (response.getCode())){

            ResponseData responseData = new ResponseUtil().setData(new ResponseDataAndTotal(response.getTotal(), response.getDetailInfoList()));
            return responseData;

        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

    @GetMapping("/order/{orderId}")
    @ApiOperation("获取指定订单的详情")
    public ResponseData getOrderDetail(@PathVariable("orderId") String orderId){
        OrderGoodsRequest request = new OrderGoodsRequest();
        request.setOrderId(orderId);
        OrderGoodsResponse response = orderQueryService.queryOrderDetail(request);
        if(ShoppingRetCode.SUCCESS.getCode().equals (response.getCode())){
            return new ResponseUtil().setData(response.getOrderGoods());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }



}
