package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IHomeService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.HomePageResponse;
import com.mall.shopping.dto.ItemCatResponse1;
import com.mall.shopping.dto.NavListResponse;
import com.mall.user.annotation.Anoymous;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/21/8:18
 */

@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "ShoppingHomeController", description = "商品模块中的首页展示")
@Anoymous
public class ShoppingHomeController {

    @Reference(retries = 0, timeout = 100000, check = false)
    IHomeService iHomeService;


    @Autowired
    private RedissonClient redissonClient;


    @ApiOperation("获取主页显示")
    @GetMapping("/homepage")
    public ResponseData getHomePage() {
        HomePageResponse homepage = iHomeService.homepage();

        RMap<String, HomePageResponse> map = redissonClient.getMap("homepage-1");

        if (map != null && map.size() != 0) {
            return new ResponseUtil().setData(map.get("homepage-1"));
        }
        if (ShoppingRetCode.DB_EXCEPTION.getCode().equals(homepage.getCode())) {
            return new ResponseUtil().setErrorMsg(homepage.getMsg());
        }
        return new ResponseUtil().setData(homepage.getPanelContentItemDtos());

    }

    @ApiOperation("导航栏显示")
    @GetMapping("/navigation")
    public ResponseData getNavigation() {

        RMap<String, NavListResponse> map = redissonClient.getMap("navigation-1");

        if (map != null && map.size() != 0) {
            return new ResponseUtil().setData(map.get("navigation-1"));
        }
        NavListResponse navigation = iHomeService.navigation();
        if (ShoppingRetCode.DB_EXCEPTION.getCode().equals(navigation.getCode())) {
            return new ResponseUtil().setErrorMsg(navigation.getMsg());
        }
        return new ResponseUtil().setData(navigation.getPannelContentDtos());

    }


    @ApiOperation("列举所有商品种类")
    @GetMapping("/categories")
    public ResponseData getCategories() {
        RMap<String, ItemCatResponse1> map = redissonClient.getMap("categories-1");
        if (map != null && map.size() != 0) {
            return new ResponseUtil().setData(map.get("categories-1"));
        }
        ItemCatResponse1 categories = iHomeService.categories();
        if (ShoppingRetCode.DB_EXCEPTION.getCode().equals(categories.getCode())) {
            return new ResponseUtil().setErrorMsg(categories.getMsg());
        }
        return new ResponseUtil().setData(categories.getItemCatDto1s());

    }


}
