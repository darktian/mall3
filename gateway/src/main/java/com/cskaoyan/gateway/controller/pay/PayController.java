package com.cskaoyan.gateway.controller.pay;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.form.pay.PayForm;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.pay.PayCoreService;
import com.mall.pay.constants.PayReturnCodeEnum;
import com.mall.pay.dto.PaymentRequest;
import com.mall.pay.dto.alipay.AlipayQueryRetResponse;
import com.mall.pay.dto.alipay.AlipaymentResponse;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:支付控制层
 * @Author: Tian
 * @Date: 2021/07/26/19:42
 */

@Slf4j
@RestController
@RequestMapping("/cashier")
@Api(tags = "PayController", description = "支付控制层")
public class PayController {

    @Reference(retries = 0,timeout = 100000, check = false)
    PayCoreService payCoreService;

    @PostMapping("/pay")
    public ResponseData pay(HttpServletRequest httpServletRequest, @RequestBody PayForm payForm) {
        PaymentRequest paymentRequest = new PaymentRequest();
        String userInfo = (String) httpServletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long userId = Long.parseLong(String.valueOf(object.get("uid")));
        paymentRequest.setUserId(userId);
        paymentRequest.setPlayerName(payForm.getNickName());
        paymentRequest.setSubject(payForm.getInfo());
        paymentRequest.setTradeNo(payForm.getOrderId());
        paymentRequest.setPayChannel("alipay");
        paymentRequest.setTotalFee(payForm.getMoney());
        paymentRequest.setOrderFee(payForm.getMoney());

        AlipaymentResponse alipaymentResponse = payCoreService.aliPay(paymentRequest);
        if(PayReturnCodeEnum.SUCCESS.getCode().equals(alipaymentResponse.getCode())){
            return new ResponseUtil().setData("http://localhost:8080/image/" + alipaymentResponse.getQrCode());
        }

        return new ResponseUtil().setErrorMsg(Integer.valueOf(alipaymentResponse.getCode()),alipaymentResponse.getMsg());
    }

    @GetMapping("/queryStatus")
    public ResponseData queryStatus(String orderId){
        AlipayQueryRetResponse alipayQueryRetResponse = payCoreService.queryAlipayRet(orderId);

        if(PayReturnCodeEnum.SUCCESS.getCode().equals(alipayQueryRetResponse.getCode())){
            return  new ResponseUtil().setData(null);
        }

        return new ResponseUtil().setErrorMsg(Integer.valueOf(alipayQueryRetResponse.getCode()),alipayQueryRetResponse.getMsg());
    }
}
