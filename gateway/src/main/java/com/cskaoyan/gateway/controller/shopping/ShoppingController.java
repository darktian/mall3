package com.cskaoyan.gateway.controller.shopping;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.*;
import com.mall.user.intercepter.TokenIntercepter;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.websocket.server.PathParam;
import java.util.Map;

@RestController
@RequestMapping("/shopping")
public class ShoppingController {

    @Reference(timeout = 30000000, check = false)//todo  3000
            ICartService iCartService;

    @GetMapping("/carts")
    public ResponseData getLogin(HttpServletRequest httpServletRequest) {
        String userInfo = (String) httpServletRequest.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        Long userId = Long.parseLong(String.valueOf(object.get("uid")));

        CartListByIdRequest cartListByIdRequest = new CartListByIdRequest();
        cartListByIdRequest.setUserId(userId);

        CartListByIdResponse cartListByIdResponse = iCartService.getCartListById(cartListByIdRequest);
        if (ShoppingRetCode.SUCCESS.getCode().equals(cartListByIdResponse.getCode())) {
            //本次调用成功
            return new ResponseUtil().setData(cartListByIdResponse.getCartProductDtos());
        }

        // 请求失败
        return new ResponseUtil().setErrorMsg(cartListByIdResponse.getMsg());

    }
    @PostMapping("/carts")
    public ResponseData addCart(@RequestBody Map<String,String> map){
        String userId = map.get("userId");
        String productId = map.get("productId");
        String productNum = map.get("productNum");

        AddCartRequest request = new AddCartRequest();
        request.setUserId(Long.valueOf(userId));
        request.setItemId(Long.valueOf(productId));
        request.setNum(Integer.valueOf(productNum));

        AddCartResponse response = iCartService.addToCart(request);
        if (ShoppingRetCode.SUCCESS.getCode().equals(response.getCode())) {
            //本次调用成功
            return new ResponseUtil().setData(response.getCode());
        }

        // 请求失败
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }
    @PutMapping("/carts")
    public ResponseData updateCart(@RequestBody Map<String,String> map){
        String userId = map.get("userId");
        String productId = map.get("productId");
        String productNum = map.get("productNum");
        String checked = map.get("checked");

        UpdateCartNumRequest request = new UpdateCartNumRequest();
        request.setUserId(Long.valueOf(userId));
        request.setItemId(Long.valueOf(productId));
        request.setNum(Integer.valueOf(productNum));
        request.setChecked(checked);

        UpdateCartNumResponse response = iCartService.updateCartNum(request);
        if (ShoppingRetCode.SUCCESS.getCode().equals(response.getCode())) {
            //本次调用成功
            return new ResponseUtil().setData(response.getCode());
        }

        // 请求失败
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }

}
