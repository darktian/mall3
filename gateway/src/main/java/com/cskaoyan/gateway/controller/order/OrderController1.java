package com.cskaoyan.gateway.controller.order;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.order.OrderCoreService;
import com.mall.order.OrderQueryService;
import com.mall.order.constant.OrderRetCode;
import com.mall.order.dto.CancelOrderRequest;
import com.mall.order.dto.CancelOrderResponse;
import com.mall.order.dto.CreateOrderRequest;
import com.mall.order.dto.CreateOrderResponse;
import com.mall.user.intercepter.TokenIntercepter;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/22/11:19
 */

@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "OrderController1", description = "订单控制层")
public class OrderController1 {

    @Reference(timeout = 3000, check = false)
    OrderQueryService orderQueryService;

    @Reference(timeout = 3000, check = false)
    OrderCoreService orderCoreService;

    @PostMapping("/cancelOrder")
    @ApiOperation("取消订单")
    public ResponseData cancelOrder(@RequestBody CancelOrderRequest cancelOrderRequest) {
        cancelOrderRequest.requestCheck();

        CancelOrderResponse cancelOrderResponse = orderQueryService.cancelOrder(cancelOrderRequest);

        if (OrderRetCode.SUCCESS.getCode().equals(cancelOrderResponse.getCode())) {
            return new ResponseUtil().setData("成功");
        }

        return new ResponseUtil().setErrorMsg(cancelOrderResponse.getMsg());
    }

    @PostMapping("/order")
    @ApiOperation("新建订单")
    public ResponseData createOrder(@RequestBody CreateOrderRequest createOrderRequest, HttpServletRequest request){

        String userInfo = (String) request.getAttribute(TokenIntercepter.USER_INFO_KEY);
        JSONObject object = JSON.parseObject(userInfo);
        long uid = Long.parseLong(String.valueOf(object.get("uid")));
        createOrderRequest.setUserId(uid);
        CreateOrderResponse response = orderCoreService.createOrder(createOrderRequest);
        if (OrderRetCode.SUCCESS.getCode().equals(response.getCode())) {
            return new ResponseUtil().setData(String.valueOf(response.getOrderId()));
        }

        return new ResponseUtil().setErrorMsg(response.getMsg());
    }
}
