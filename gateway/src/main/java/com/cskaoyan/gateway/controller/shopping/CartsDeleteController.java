package com.cskaoyan.gateway.controller.shopping;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

/**
 * @Description:
 * @Author: peng
 * @Date: 2021/07/21/8:18
 */

@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "CartsDeleteController", description = "删除购物⻋中的商品")
public class CartsDeleteController {

    //远程服务调用
    @Reference(timeout = 300000,check = false)
    ICartService icartService;

    @ApiOperation("删除购物车中的商品")
    //请求方法是delete
    @RequestMapping(value="/carts/{userId}/{itemId}",method = RequestMethod.DELETE)
    public ResponseData deleteCartsItem(@PathVariable("userId") Long userId, @PathVariable("itemId")Long itemId){
        //新建接收对象request
        DeleteCartItemRequest request = new DeleteCartItemRequest();
        //数据封装到request Long userId , Long itemId;
        request.setUserId(userId);
        request.setItemId(itemId);
        //远程服务调用,传入封装的req
        DeleteCartItemResponse response = icartService.deleteCartItem(request);

        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())) {
            return new ResponseUtil().setData(response.getMsg());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());

    }

    @ApiOperation("删除购物车中选中的商品")//http://www.maimaimai.ltd/shopping/items/62
    @RequestMapping(value="/items/{userId}",method = RequestMethod.DELETE)//请求方法是delete
    public ResponseData deleteSomeCartsItems(@PathVariable("userId") Long userId){
        //new 接收对象request
        DeleteCheckedItemRequest request = new DeleteCheckedItemRequest();
        //数据封装到req
        request.setUserId(userId);
        //远程服务调用
        DeleteCheckedItemResposne response = icartService.deleteCheckedItem(request);

        if (response.getCode().equals(ShoppingRetCode.SUCCESS.getCode())) {
            return new ResponseUtil().setData(response.getMsg());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());


    }

}
