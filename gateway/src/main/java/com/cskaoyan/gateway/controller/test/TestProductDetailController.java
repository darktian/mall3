package com.cskaoyan.gateway.controller.test;

import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.ITestProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.TestProductDetailDto;
import com.mall.shopping.dto.TestProductDetailRequest;
import com.mall.shopping.dto.TestProductDetailResponse;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestProductDetailController {

    @Reference(retries = 0, timeout = 3000, check = false)
    ITestProductService testProductService;

    @GetMapping("/product")
    @Anoymous
    public ResponseData getProductDetail(Long productId) {
        // 需要将请求参数，封装到一个xxxRequest对象中

        TestProductDetailRequest testProductDetailRequest = new TestProductDetailRequest();
        testProductDetailRequest.setProductId(productId);

       TestProductDetailResponse response = testProductService.getProductDetail(testProductDetailRequest);
       if (ShoppingRetCode.SUCCESS.getCode().equals(response.getCode())) {
           //本次调用成功
           return new ResponseUtil().setData(response.getProductDetailDto());
       }

        // 请求失败
        return new ResponseUtil().setErrorMsg(response.getMsg());
    }
}
