package com.cskaoyan.gateway.controller.test;


import com.mall.user.annotation.Anoymous;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping("/intercept")
    @Anoymous
    public String testTokenIntercept() {
        return "hello, intercept";
    }
}
