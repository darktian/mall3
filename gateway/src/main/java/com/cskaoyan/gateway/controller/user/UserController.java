package com.cskaoyan.gateway.controller.user;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cskaoyan.gateway.form.user.RegisterUser;
import com.cskaoyan.gateway.form.user.UserInfo;
import com.cskaoyan.gateway.form.user.UserLogin;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseUtil;
import com.mall.commons.tool.utils.CookieUtil;
import com.mall.user.IKaptchaService;
import com.mall.user.IUserLoginService;
import com.mall.user.annotation.Anoymous;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dto.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*
 *@Author: WUW
 *@Description
 *
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Reference(retries = 1, timeout = 10000, check = false)
    IUserLoginService iUserLoginService;

    @Reference(retries = 1, timeout = 10000, check = false)
    IKaptchaService kaptchaService;


    @GetMapping("/login")
    public ResponseData getLogin(HttpServletRequest httpServletRequest) {
        CheckAuthRequest checkAuthRequest = new CheckAuthRequest();
        String token = CookieUtil.getCookieValue(httpServletRequest, "access_token");
        if (StringUtils.isEmpty(token)) {
            return new ResponseUtil<>().setErrorMsg("token失效");
        }
        checkAuthRequest.setToken(token);
        CheckAuthResponse checkAuthResponse = iUserLoginService.validToken(checkAuthRequest);
        if ((SysRetCodeConstants.SUCCESS.getCode().equals(checkAuthResponse.getCode()))&&(checkAuthResponse.getUserinfo() != null)) {
            UserInfo userInfo = new UserInfo();
            JSONObject jsonObject = null;
            try {
                System.out.println(checkAuthResponse.getUserinfo() + "+++++++++++");
                jsonObject = JSON.parseObject(checkAuthResponse.getUserinfo());
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseUtil<>().setErrorMsg("token失效");
            }
            String username = String.valueOf(jsonObject.get("username"));
            MemberResponse memberResponse = iUserLoginService.getUserInfo(username);
            BeanUtils.copyProperties(memberResponse, userInfo);
            return new ResponseUtil<>().setData(userInfo);
        } else {
            return new ResponseUtil<>().setErrorMsg("token无效");
        }
    }

    @Anoymous
    @PostMapping("/login")
    public ResponseData postLogin(@RequestBody UserLogin userLogin, HttpServletRequest httpServletRequest, HttpServletResponse response) {
        if (userLogin == null || "".equals(userLogin.getCaptcha().trim()) ||"".equals(userLogin.getUserName().trim())||"".equals(userLogin.getUserPwd().trim())) {
            return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.REQUISITE_PARAMETER_NOT_EXIST.getMessage());
        }
        KaptchaCodeRequest kaptchaCodeRequest = new KaptchaCodeRequest();
        String uuid = CookieUtil.getCookieValue(httpServletRequest, "kaptcha_uuid");
        kaptchaCodeRequest.setCode(userLogin.getCaptcha());
        kaptchaCodeRequest.setUuid(uuid);
        KaptchaCodeResponse kaptchaCodeResponse = kaptchaService.validateKaptchaCode(kaptchaCodeRequest);
        System.out.println("5555555555555555");
        if (SysRetCodeConstants.KAPTCHA_CODE_ERROR.getCode().equals(kaptchaCodeResponse.getCode())) {
            return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.KAPTCHA_CODE_ERROR.getMessage());
        }
        UserLoginRequest userLoginRequest = new UserLoginRequest();
        BeanUtils.copyProperties(userLogin, userLoginRequest);
        userLoginRequest.setPassword(userLogin.getUserPwd());
        UserLoginResponse userLoginResponse = iUserLoginService.login(userLoginRequest);
        if (SysRetCodeConstants.SUCCESS.getCode().equals(userLoginResponse.getCode())) {
            UserInfo userInfo = new UserInfo();
            BeanUtils.copyProperties(userLoginResponse, userInfo);
            userInfo.setUid(userLoginResponse.getId());
            String s = JSON.toJSONString(userInfo);
            String tokens = iUserLoginService.getTokens(s);
            Cookie cookie = CookieUtil.genCookie("access_token", tokens, "/", 3000);
            cookie.setHttpOnly(true);
            response.addCookie(cookie);
            userLoginResponse.setToken(tokens);
            return new ResponseUtil<>().setData(userLoginResponse);
        }
        return new ResponseUtil<>().setErrorMsg(userLoginResponse.getMsg());
    }

    @Anoymous
    @PostMapping("/register")
    public ResponseData register(@RequestBody RegisterUser registerUser, HttpServletRequest httpServletRequest, HttpServletResponse response) {
        if (registerUser == null || "".equals(registerUser.getCaptcha().trim()) ||"".equals(registerUser.getUserName().trim())||"".equals(registerUser.getUserPwd().trim())|| (!registerUser.getEmail().endsWith(".com"))) {
            return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.REQUISITE_PARAMETER_NOT_EXIST.getMessage());
        }
        KaptchaCodeRequest kaptchaCodeRequest = new KaptchaCodeRequest();
        String uuid = CookieUtil.getCookieValue(httpServletRequest, "kaptcha_uuid");
        kaptchaCodeRequest.setCode(registerUser.getCaptcha());
        kaptchaCodeRequest.setUuid(uuid);
        KaptchaCodeResponse kaptchaCodeResponse = kaptchaService.validateKaptchaCode(kaptchaCodeRequest);
        if (SysRetCodeConstants.KAPTCHA_CODE_ERROR.getCode().equals(kaptchaCodeResponse.getCode())) {
            return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.KAPTCHA_CODE_ERROR.getMessage());
        }
        UserRegisterRequest userRegisterRequest = new UserRegisterRequest();
        BeanUtils.copyProperties(registerUser, userRegisterRequest);
        UserRegisterResponse userRegisterResponse = iUserLoginService.register(userRegisterRequest);
        if (SysRetCodeConstants.SUCCESS.getCode().equals(userRegisterResponse.getCode())) {
            return new ResponseUtil<>().setData("登录成功，请尽快绑定邮箱进行激活");
        }
        return new ResponseUtil<>().setErrorMsg(userRegisterResponse.getMsg());
    }

    @GetMapping("/loginOut")
    public ResponseData logout(HttpServletRequest httpServletRequest, HttpServletResponse response) {
        Cookie cookie = new Cookie("access_token", "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        response.addCookie(cookie);
        return new ResponseUtil<>().setData(null);
    }

    @Anoymous
    @GetMapping("/verify")
    public ResponseData verify(UserVerifyRequest userVerifyRequest) {
        if (userVerifyRequest == null || "".equals(userVerifyRequest.getUserName().trim()) ||"".equals(userVerifyRequest.getUuid().trim())) {
            return new ResponseUtil<>().setErrorMsg(SysRetCodeConstants.REQUISITE_PARAMETER_NOT_EXIST.getMessage());
        }
        UserVerifyResponse response = iUserLoginService.verifyActive(userVerifyRequest);
        if (SysRetCodeConstants.SUCCESS.getCode().equals(response.getCode())) {
            return new ResponseUtil<>().setData(null);
        }
        return new ResponseUtil<>().setErrorMsg(response.getMsg());
    }

}
