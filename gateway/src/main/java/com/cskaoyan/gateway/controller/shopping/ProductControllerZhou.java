package com.cskaoyan.gateway.controller.shopping;

import com.cskaoyan.gateway.form.shopping.GoodsFormZhou;
import com.mall.commons.result.ResponseData;
import com.mall.commons.result.ResponseDataAndTotal;
import com.mall.commons.result.ResponseUtil;
import com.mall.shopping.IProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dto.*;
import com.mall.user.annotation.Anoymous;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 */
@Slf4j
@RestController
@RequestMapping("/shopping")
@Api(tags = "ProdectController", description = "商品层")
public class ProductControllerZhou {

    @Reference(timeout = 300000,check = false)
    IProductService iProductService;


    @Anoymous
    @GetMapping("/product/{id}")
    @ApiOperation("根据商品id获取商品的信息")
    public ResponseData getProductById(@PathVariable("id") Long id) {
        ProductDetailRequest productDetailRequest = new ProductDetailRequest();
        productDetailRequest.setId(id);
        ProductDetailResponse response = iProductService.getProductDetail(productDetailRequest);
        if(ShoppingRetCode.SUCCESS.getCode().equals (response.getCode())){
            return new ResponseUtil().setData(response.getProductDetailDto());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());

    }

    @Anoymous
    @GetMapping("/goods")
    @ApiOperation("获取首页的商品")
    public ResponseData goodsList(GoodsFormZhou goodsForm) {

        System.out.println("goodslist");
        AllProductRequestZhou productRequest = goodsForm.goodsForm2Request(goodsForm);
        AllProductResponse response = iProductService.getAllProduct(productRequest);
        if(ShoppingRetCode.SUCCESS.getCode().equals (response.getCode())){
            ResponseData responseData = new ResponseUtil().setData(new ResponseDataAndTotal(response.getTotal(), response.getProductDtoList()));
            //todo 结果封装已经修改，但是前端没有进行修改
            return responseData;
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());

    }

    @Anoymous
    @GetMapping("/recommend")
    @ApiOperation("获取推荐商品")
    public ResponseData recommend() {
        System.out.println("7777777777777777777");
        RecommendResponse response = iProductService.getRecommendGoods();
        if(ShoppingRetCode.SUCCESS.getCode().equals (response.getCode())){
            return  new ResponseUtil().setData(response.getPanelContentItemDtos());
        }
        return new ResponseUtil().setErrorMsg(response.getMsg());


    }


}
