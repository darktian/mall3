package com.mall.pay.services;

import com.mall.order.OrderQueryService;
import com.mall.pay.PayCoreService;
import com.mall.pay.constants.PayResultEnum;
import com.mall.pay.constants.PayReturnCodeEnum;
import com.mall.pay.dal.entitys.Payment;
import com.mall.pay.dal.persistence.PaymentMapper;
import com.mall.pay.dto.PaymentRequest;
import com.mall.pay.dto.alipay.AlipayQueryRetResponse;
import com.mall.pay.dto.alipay.AlipaymentResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PreDestroy;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class PayCoreServiceImpl implements PayCoreService {
    AlipaymentResponse alipaymentResponse = new AlipaymentResponse();
    @Autowired
    PayHelper payHelper;

    @Autowired
    PaymentMapper paymentMapper;

    @Reference(timeout = 3000, check = false)
    OrderQueryService orderQueryService;





    @Override
    public AlipaymentResponse aliPay(PaymentRequest request) {

        // 1.  使用支付宝sdk的功能去请求，支付二维码

        List<String> test_trade_precreate = payHelper.test_trade_precreate(request);
        String qrFileName = test_trade_precreate.get(0);
        String payNo = test_trade_precreate.get(1);
        if (qrFileName == null) {
            // 请求支付二维码失败
            alipaymentResponse.setMsg(PayReturnCodeEnum.GET_CODE_FALIED.getMsg());
            alipaymentResponse.setCode(PayReturnCodeEnum.GET_CODE_FALIED.getCode());

        } else {
            // 否则，请求到支付二维码
            // 2.  在本地插入tb_payment，插入一条本次订单的一条支付记录

            Payment payment = new Payment();
            payment.setStatus(PayResultEnum.PAY_PROCESSING.getCode());
            payment.setOrderId(request.getTradeNo());
            payment.setProductName(request.getSubject());
            payment.setPayerAmount(request.getTotalFee());
            payment.setPayNo(payNo);
            payment.setPayerUid(request.getUserId());
            payment.setPayerName(request.getPlayerName());
            payment.setOrderAmount(request.getOrderFee());
            payment.setPayWay("ali_pay");
            payment.setRemark("支付宝支付");
            payment.setCreateTime(new Date());
            payment.setUpdateTime(new Date());
            paymentMapper.insert(payment);

            alipaymentResponse.setQrCode(qrFileName);
            alipaymentResponse.setMsg(PayReturnCodeEnum.SUCCESS.getMsg());
            alipaymentResponse.setCode(PayReturnCodeEnum.SUCCESS.getCode());
        }


        return alipaymentResponse;
    }

    @Override
    public AlipayQueryRetResponse queryAlipayRet(String orderId) {
        AlipayQueryRetResponse alipayQueryRetResponse = new AlipayQueryRetResponse();
        boolean isSuccess = payHelper.test_trade_query(orderId);
     try {
         if (isSuccess) {
             // 支付成功
             // 1. 修改本订单对应的支付记录： 修改为支付成功
             paymentMapper.updateSuccessStatusByOrderId(orderId);
             // 2. 修改订单状态，为已支付状态
             orderQueryService.updatePayCount(orderId);
             // 3. 修改库存，将订单中商品对应的库存，锁定库存清零
             // 4.  修改订单商品条目，对应的库存状态：3 成功扣减库存
             alipayQueryRetResponse.setCode(PayReturnCodeEnum.SUCCESS.getCode());
             alipayQueryRetResponse.setMsg(PayReturnCodeEnum.SUCCESS.getMsg());
         } else {
             // 没有成功支付
             // 修改本订单对应的支付记录： 修改为支付失败
             paymentMapper.updateFailStatusByOrderId(orderId);
             alipayQueryRetResponse.setCode(PayReturnCodeEnum.PAYMENT_PROCESSOR_FAILED.getCode());
             alipayQueryRetResponse.setMsg(PayReturnCodeEnum.PAYMENT_PROCESSOR_FAILED.getMsg());
         }
     }catch (Exception e){
         alipayQueryRetResponse.setCode(PayReturnCodeEnum.DB_EXCEPTION.getCode());
         alipayQueryRetResponse.setMsg(PayReturnCodeEnum.DB_EXCEPTION.getMsg());
     }
        return alipayQueryRetResponse;
    }
}
