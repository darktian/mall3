package com.mall.promo.mq;

import com.alibaba.fastjson.JSON;
import com.mall.order.dto.CreateSeckillOrderRequest;
import com.mall.promo.cache.CacheManager;
import com.mall.promo.dal.persistence.PromoItemMapper;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.LocalTransactionState;
import org.apache.rocketmq.client.producer.TransactionListener;
import org.apache.rocketmq.client.producer.TransactionMQProducer;
import org.apache.rocketmq.client.producer.TransactionSendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

@Component
public class PromoOrderProducer {

    TransactionMQProducer producer;

    @Autowired
    PromoItemMapper itemMapper;

    @Autowired
    CacheManager cacheManager;

    @Autowired
    RedissonClient redissonClient;



    @PostConstruct
    public void init() {
        producer = new TransactionMQProducer("promo_order_producer");

        producer.setNamesrvAddr("127.0.0.1:9876");


        producer.setTransactionListener(new TransactionListener() {
            @Override
            public LocalTransactionState executeLocalTransaction(Message msg, Object arg) {
                // 扣减秒杀商品库存  psId  productId
                Map<String, Long> paramMap = (Map<String, Long>) arg;
                Long productId = paramMap.get("productId");
                Long psId = paramMap.get("psId");

                // 使用基于redis的分布式锁
                String lockKey = "promo_item_stock_lock_" + psId + "_" + productId;
                RLock distributedLock = redissonClient.getLock(lockKey);
                String transactionkey = "promo_order_local_transaction_" + msg.getTransactionId();

                Integer effectiveRow = null;
                //  加锁失败，加锁失败的线程会阻塞等待
                distributedLock.lock();
                try {
                    effectiveRow = itemMapper.decreaseStock(productId, psId);
                } finally {
                    distributedLock.unlock();
                }

                if (effectiveRow < 1) {
                    // 扣减库存失败
                    cacheManager.setCache(transactionkey, "fail", 1);
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }


                // 扣减库存成功
                cacheManager.setCache(transactionkey, "success", 1);
                return LocalTransactionState.COMMIT_MESSAGE;
            }

            @Override
            public LocalTransactionState checkLocalTransaction(MessageExt msg) {

                String transactionKey = "promo_order_local_transaction_" + msg.getTransactionId();

                String s = cacheManager.checkCache(transactionKey);
                if (s != null && "success".equals(s.trim())) {
                    return LocalTransactionState.COMMIT_MESSAGE;
                }

                if (s != null && "fail".equals(s.trim())) {
                    return LocalTransactionState.ROLLBACK_MESSAGE;
                }

                return LocalTransactionState.UNKNOW;
            }
        });

        try {
            producer.start();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }


    public boolean sendCreatePromoOrderMessage(CreateSeckillOrderRequest request, Long psId, Long productId) {
        // 准备发送事物消息
        // 1. 准备待发送的消息
        String s = JSON.toJSONString(request);
        Message message = new Message("create_promotion_order", s.getBytes(Charset.forName("utf-8")));

        //  准备本地事物执行所需的参数
        Map<String, Long> paramMap = new HashMap<>();
        paramMap.put("productId", productId);
        paramMap.put("psId", psId);


        // 2. 发送事物消息
        TransactionSendResult sendResult;
        try {
            sendResult = producer.sendMessageInTransaction(message, paramMap);
            if (sendResult != null && LocalTransactionState.COMMIT_MESSAGE.equals(sendResult.getLocalTransactionState())) {
                //  打印日志： 消息发送成功，且本地事物执行成功
                return true;
            } else if ( sendResult != null && LocalTransactionState.ROLLBACK_MESSAGE.equals(sendResult.getLocalTransactionState())){
               // 打印日志:  消息发送成功，但本地事物执行失败
                return false;
            }
        } catch (MQClientException e) {
            e.printStackTrace();
        }

        return false;
    }


}
