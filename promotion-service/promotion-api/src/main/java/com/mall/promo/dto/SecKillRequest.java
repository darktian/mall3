package com.mall.promo.dto;

import lombok.Data;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/28/20:43
 */
@Data
public class SecKillRequest {

    private Long psId;
    private Long productId;
    private Long addressId;
    private String tel;
    private String streetName;
    private String userName;
}
