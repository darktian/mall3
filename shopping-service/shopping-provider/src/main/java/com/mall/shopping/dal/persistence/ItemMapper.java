package com.mall.shopping.dal.persistence;

import com.mall.shopping.dal.entitys.Item;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.math.BigDecimal;
import java.util.List;

public interface ItemMapper extends Mapper<Item> {

    List<Item> selectItemFront(@Param("cid") Long cid,
                               @Param("orderCol") String orderCol, @Param("orderDir") String orderDir,
                               @Param("priceGt") Integer priceGt, @Param("priceLte") Integer priceLte);

    List<Item> selectItem(
            @Param("cid") Long cid,
            @Param("sort") Integer sort,
            @Param("size") Integer size,
            @Param("page") Integer page,
            @Param("priceGt") BigDecimal priceGt,
            @Param("priceLte") BigDecimal priceLte);

    Long selectItemCount(
            @Param("cid") Long cid,
            @Param("priceGt") BigDecimal priceGt,
            @Param("priceLte") BigDecimal priceLte);
}