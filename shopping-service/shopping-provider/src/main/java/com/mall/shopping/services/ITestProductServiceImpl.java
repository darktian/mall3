package com.mall.shopping.services;

import com.mall.shopping.ITestProductService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.TestProductDetailConverter;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.TestProductDetailDto;
import com.mall.shopping.dto.TestProductDetailRequest;
import com.mall.shopping.dto.TestProductDetailResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
@Slf4j
public class ITestProductServiceImpl implements ITestProductService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    TestProductDetailConverter productDetailConverter;

    @Override
    public TestProductDetailResponse getProductDetail(TestProductDetailRequest productDetailRequest) {

        TestProductDetailResponse testProductDetailResponse = new TestProductDetailResponse();
        try {
            System.out.println("调用到了getProductDetail");
            // 校验请求参数是否合法
            productDetailRequest.requestCheck();

            Item item = itemMapper.selectByPrimaryKey(productDetailRequest.getProductId());
            TestProductDetailDto testProductDetailDto = productDetailConverter.testProductDetailDoToDTo(item);
            testProductDetailResponse.setProductDetailDto(testProductDetailDto);
            testProductDetailResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            testProductDetailResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ITestProductServiceImpl#getProductDetail " + e);
            testProductDetailResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            testProductDetailResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return testProductDetailResponse;
    }
}
