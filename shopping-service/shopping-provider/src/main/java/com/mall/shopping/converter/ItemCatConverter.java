package com.mall.shopping.converter;

import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dto.ItemCatDto1;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/21/11:02
 */
@Mapper(componentModel = "spring")
public interface ItemCatConverter {
    @Mappings({
            @Mapping(source = "icon", target = "iconUrl"),
            @Mapping(source = "isParent",target = "parent")
    })
    ItemCatDto1 itemCat1ToitemCatDto(ItemCat itemCat);

    @Mappings({})
    List<ItemCatDto1> itemCat1ToitemCatDtoList(List<ItemCat> itemCats);
}
