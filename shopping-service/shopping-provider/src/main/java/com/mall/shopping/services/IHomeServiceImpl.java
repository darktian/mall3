package com.mall.shopping.services;

import com.github.pagehelper.PageHelper;
import com.mall.shopping.IHomeService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.converter.ItemCatConverter;
import com.mall.shopping.dal.entitys.ItemCat;
import com.mall.shopping.dal.entitys.Panel;
import com.mall.shopping.dal.entitys.PanelContent;
import com.mall.shopping.dal.entitys.PanelContentItem;
import com.mall.shopping.dal.persistence.ItemCatMapper;
import com.mall.shopping.dal.persistence.PanelContentMapper;
import com.mall.shopping.dal.persistence.PanelMapper;
import com.mall.shopping.dto.*;
import com.mall.shopping.services.cache.CacheManager;
import org.apache.dubbo.config.annotation.Service;
import org.checkerframework.checker.units.qual.C;
import org.redisson.Redisson;
import org.redisson.api.RBatch;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/21/9:26
 */
@Service
public class IHomeServiceImpl implements IHomeService {

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ContentConverter contentConverter;

    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    ItemCatMapper itemCatMapper;

    @Autowired
    ItemCatConverter itemCatConverter;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public HomePageResponse homepage() {
        RMap<String, HomePageResponse> map = redissonClient.getMap("homepage-1");

        HomePageResponse homePageResponse = new HomePageResponse();


        HashSet<PanelDto> panelDtos = new HashSet<>();
        try {
            Panel selectPanel = new Panel();
            selectPanel.setPosition(0);
            List<Panel> panels = panelMapper.select(selectPanel);
            List<PanelDto> panelDtos1 = contentConverter.panen2DtoList(panels);
            for (PanelDto panelDto : panelDtos1) {
                List<PanelContentItem> panelContentItems = panelContentMapper.selectPanelContentAndProductWithPanelId(panelDto.getId());
                List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
                panelDto.setPanelContentItems(panelContentItemDtos);
            }
            panelDtos.addAll(panelDtos1);
        } catch (Exception e) {
            homePageResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            homePageResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        homePageResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
        homePageResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        homePageResponse.setPanelContentItemDtos(panelDtos);

        map.put("homepage-1", homePageResponse);
        return homePageResponse;
    }

    @Override
    public NavListResponse navigation() {
        RMap<String, NavListResponse> map = redissonClient.getMap("navigation-1");


        NavListResponse navListResponse = new NavListResponse();

        PanelContent panelContent = new PanelContent();
        panelContent.setPanelId(0);
        try {
            List<PanelContent> select = panelContentMapper.select(panelContent);

            List<PanelContentDto> panelContentDtos = contentConverter.panelContents2Dto(select);
            navListResponse
                    .setPannelContentDtos(panelContentDtos);
        } catch (Exception e) {
            navListResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            navListResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        navListResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
        navListResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        map.put("navigation-1", navListResponse);
        return navListResponse;
    }

    @Override
    public ItemCatResponse1 categories() {
        RMap<String, ItemCatResponse1> map = redissonClient.getMap("categories-1");

        ItemCatResponse1 itemCatResponse1 = new ItemCatResponse1();

        List<ItemCatDto1> itemCatDto1s = null;
        try {
            ItemCat itemCat = new ItemCat();
            itemCat.setStatus(1);
            List<ItemCat> itemCats = itemCatMapper.select(itemCat);
            itemCatDto1s = itemCatConverter.itemCat1ToitemCatDtoList(itemCats);
        } catch (Exception e) {
            itemCatResponse1.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            itemCatResponse1.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        itemCatResponse1.setCode(ShoppingRetCode.SUCCESS.getCode());
        itemCatResponse1.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        itemCatResponse1.setItemCatDto1s(itemCatDto1s);
        map.put("categories-1", itemCatResponse1);
        return itemCatResponse1;
    }
}
