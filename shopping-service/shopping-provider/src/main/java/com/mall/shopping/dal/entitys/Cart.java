package com.mall.shopping.dal.entitys;

import lombok.Data;

import java.io.Serializable;

@Data
public class Cart implements Serializable {
    Integer  num;
    String checked;
}
