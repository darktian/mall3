package com.mall.shopping.services;

import com.mall.shopping.ICartService;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.dal.entitys.Cart;
import com.mall.shopping.dal.entitys.Item;
import com.mall.shopping.dal.persistence.ItemMapper;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.redisson.Redisson;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author liangJiapeng
 * @create 2021-07-21 15:39
 * describe:
 */

@Slf4j
@Service
public class ICartServiceImpl implements ICartService {

    @Autowired
    ItemMapper itemMapper;

    @Override
    public CartListByIdResponse getCartListById(CartListByIdRequest request) {//获得购物车列表
        CartListByIdResponse response = new CartListByIdResponse();
        try {
            System.out.println("getCartListById");
            // 校验请求参数是否合法
            request.requestCheck();
            Long userId = request.getUserId();
            //从redis数据库查询userId对应的购物车列表-itemId||个数
            Config config = new Config();
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            RedissonClient redissonClient = Redisson.create(config);

            RMap<Long, com.mall.shopping.dal.entitys.Cart> redissonClientMap = redissonClient.getMap(userId.toString());//userId-map(productId-num)

            com.mall.shopping.dal.entitys.Cart test = new com.mall.shopping.dal.entitys.Cart();
            List<CartProductDto> list = new ArrayList<>();
            Set<Long> longs = redissonClientMap.keySet();
            for(Long i : longs){
                com.mall.shopping.dal.entitys.Cart cart = redissonClientMap.get(i);
                CartProductDto cartProductDto = new CartProductDto();
                cartProductDto.setProductId(i);
                cartProductDto.setProductNum(cart.getNum().longValue());
                cartProductDto.setChecked(cart.getChecked());
                list.add(cartProductDto);
            }
            //System.out.println(a.name);
            //从商品列表查询商品相关信息
            for(CartProductDto i : list){
                Item item = itemMapper.selectByPrimaryKey(i.getProductId());
                i.setLimitNum((Long)item.getLimitNum().longValue());
                i.setProductImg(item.getImageBig());
                i.setProductName(item.getTitle());
                i.setSalePrice(item.getPrice());
            }

            response.setCartProductDtos(list);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ICartServiceImpl#getCartListById " + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }

    @Override
    public AddCartResponse addToCart(AddCartRequest request) {//添加商品到购物车

        AddCartResponse response = new AddCartResponse();
        try {
            System.out.println("addToCart");
            // 校验请求参数是否合法
            request.requestCheck();
            Long userId = request.getUserId();
            //向redis数据库添加新数据
            Config config = new Config();
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            RedissonClient redissonClient = Redisson.create(config);

            RMap<Long, com.mall.shopping.dal.entitys.Cart> redissonClientMap = redissonClient.getMap(userId.toString());//userId-map(productId-num)

            com.mall.shopping.dal.entitys.Cart test = new com.mall.shopping.dal.entitys.Cart();
            test.setChecked("false");
            test.setNum(request.getNum());
            com.mall.shopping.dal.entitys.Cart put = redissonClientMap.put(request.getItemId(), test);
            System.out.println("插入成功" + put);

            //System.out.println(a.name);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ICartServiceImpl#getCartListById " + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }

    @Override
    public UpdateCartNumResponse updateCartNum(UpdateCartNumRequest request){
        UpdateCartNumResponse response = new UpdateCartNumResponse();
        try {
            System.out.println("updateCartNum");
            // 校验请求参数是否合法
            request.requestCheck();
            Long userId = request.getUserId();
            //向redis数据库更新数据
            Config config = new Config();
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            RedissonClient redissonClient = Redisson.create(config);

            RMap<Long, com.mall.shopping.dal.entitys.Cart> redissonClientMap = redissonClient.getMap(userId.toString());//userId-map(productId-num)

            com.mall.shopping.dal.entitys.Cart cart = redissonClientMap.get(request.getItemId());
            cart.setNum(request.getNum());
            cart.setChecked(request.getChecked());
            redissonClientMap.put(request.getItemId(), cart);

            //System.out.println(a.name);
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ICartServiceImpl#getCartListById " + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }

    @Override
    public CheckAllItemResponse checkAllCartItem(CheckAllItemRequest request) {
        return null;
    }

    @Autowired
    RedissonClient redissonClient;

    /**
     * 删除购物车中的商品
     *
     * @param request
     * @return
     */
    @Override
    public DeleteCartItemResponse deleteCartItem(DeleteCartItemRequest request) {
        //new一个要响应的vo
        DeleteCartItemResponse response = new DeleteCartItemResponse();
        try {
            //参数检验
            request.requestCheck();
            //购物车的数据与redis数据库交互
            // 1. new 出一个config对象
            Config config = new Config();
            // 2. 配置Config对象
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            // 3. 创建redis客户端
            RedissonClient redissonClient = Redisson.create(config);
            //从客户端取对应的用户userId的购物车数据map
            RMap<Long, Cart> redissonClientMap = redissonClient.getMap(request.getUserId().toString());

            //根据itemid从购物车map里删除商品
            redissonClientMap.remove(request.getItemId());
            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ICartServiceImpl#deleteCartItem" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }


        return response;
    }

    @Override
    public DeleteCheckedItemResposne deleteCheckedItem(DeleteCheckedItemRequest request) {
        //new一个要响应的vo
        DeleteCheckedItemResposne response = new DeleteCheckedItemResposne();
        try {
            //参数校验
            request.requestCheck();
            // 购物车的数据与redis数据库交互
            // 1. new 出一个config对象
            Config config = new Config();
            // 2. 配置Config对象
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            // 3.创建redis客户端
            RedissonClient redissonClient = Redisson.create(config);
            //从客户端取对应的用户userId的购物车数据map
            RMap<Long, com.mall.shopping.dal.entitys.Cart> redissonClientMap = redissonClient.getMap(request.getUserId().toString());
            Set<Long> longs = redissonClientMap.keySet();
            for (Long aLong : longs) {
                com.mall.shopping.dal.entitys.Cart cart = redissonClientMap.get(aLong);
//                com.mall.shopping.dal.entitys.Cart cart = (com.mall.shopping.dal.entitys.Cart) cart1;
                if ("true".equals(cart.getChecked())) {
                    redissonClientMap.remove(aLong);
                }
            }

            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
         } catch (Exception e) {
            log.info("ICartServiceImpl#deleteCheckedItem" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }


        return response;
    }

    @Override
    public ClearCartItemResponse clearCartItemByUserID(ClearCartItemRequest request) {
        //new一个要响应的vo
        ClearCartItemResponse response = new ClearCartItemResponse();
        try {
            //参数校验
            request.requestCheck();
            // 购物车的数据与redis数据库交互
            // 1. new 出一个config对象
            Config config = new Config();
            // 2. 配置Config对象
            SingleServerConfig serverConfig = config.useSingleServer()
                    .setAddress("redis://localhost:6379");
            // 3.创建redis客户端
            RedissonClient redissonClient = Redisson.create(config);
            //从客户端取对应的用户userId的购物车数据map
            RMap<Long, Cart> redissonClientMap = redissonClient.getMap(request.getUserId().toString());
            //Set<Long> longs = redissonClientMap.keySet();
            //for (Long aLong : longs) {
            //Cart cart = redissonClientMap.get(aLong);
            //if("true".equals(cart.getChecked())){
            //redissonClientMap.remove(aLong);
            Iterator<Long> iterator = request.getProductIds().iterator();
            while (iterator.hasNext()) {
                redissonClientMap.remove(iterator.next());
            }

            response.setCode(ShoppingRetCode.SUCCESS.getCode());
            response.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("ICartServiceImpl#clearCartItemByUserID" + e);
            response.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            response.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return response;
    }
}
