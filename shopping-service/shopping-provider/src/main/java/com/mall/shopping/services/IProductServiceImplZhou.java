package com.mall.shopping.services;

import com.mall.shopping.IProductService;
import com.mall.shopping.constant.GlobalConstants;
import com.mall.shopping.constants.ShoppingRetCode;
import com.mall.shopping.converter.ContentConverter;
import com.mall.shopping.converter.ProduceDetailConverterZhou;
import com.mall.shopping.converter.ProductConverter;
import com.mall.shopping.dal.entitys.*;
import com.mall.shopping.dal.persistence.*;
import com.mall.shopping.dto.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 *
 */
@Slf4j
@Service
public class IProductServiceImplZhou implements IProductService {

    @Autowired
    ItemMapper itemMapper;

    @Autowired
    ItemCatMapper itemCatMapper;

    @Autowired
    ItemDescMapper itemDescMapper;

    @Autowired
    PanelContentMapper panelContentMapper;

    @Autowired
    PanelMapper panelMapper;

    @Autowired
    ProduceDetailConverterZhou produceDetailConverterZhou;

    @Autowired
    ContentConverter contentConverter;

    @Autowired
    ProductConverter productConverter;

    /**
     * 根据productID 获取productDetail
     * @param request
     * @return
     */
    @Override
    public ProductDetailResponse getProductDetail(ProductDetailRequest request) {
        ProductDetailResponse productDetailResponse = new ProductDetailResponse();

        try{
            request.requestCheck();

            //在商品类目中找一个图片 分类的图片 根据 item -cid 来获取 productImageBig
            //商品`tb_item_desc` 找到一个item-desc - detail
            Item item = itemMapper.selectByPrimaryKey(request.getId());
            ItemCat itemCat = itemCatMapper.selectByPrimaryKey(item.getCid());
            ItemDesc itemDesc = itemDescMapper.selectByPrimaryKey(request.getId());
            ProductDetailDto productDetailDto = produceDetailConverterZhou.ProductDetailDoToDTo(item);
            //对其中的图片进行处理
                        //  将prodectImageSmall 转化为string 数组

            productDetailDto.setProductName(itemCat.getName()); //封装productName
            productDetailDto.setDetail(itemDesc.getItemDesc());
            productDetailDto.setProductImageSmall(Arrays.asList(item.getImages()));
            productDetailDto.setProductImageBig(itemCat.getIcon());



            if(itemCat != null) {
                productDetailDto.setProductName(itemCat.getName());
                productDetailDto.setProductImageBig(itemCat.getIcon());
            }
            if(itemDesc != null) productDetailDto.setDetail(itemDesc.getItemDesc());
            productDetailDto.setProductImageSmall(Arrays.asList(item.getImages()));

            //进行数据封装
            productDetailResponse .setProductDetailDto(productDetailDto);
            productDetailResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        }catch (Exception e ){
            log.info("IProductServiceImplZhou#getProductDetail " + e);
            productDetailResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            productDetailResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return productDetailResponse;
    }

    //获取所有的商品信息并且进行分页
    @Override
    public AllProductResponse getAllProduct(AllProductRequestZhou request) {
        //进行数据校验;
        AllProductResponse allProductResponse = new AllProductResponse();

        try {
            request.requestCheck();
            List<Item> items = itemMapper.selectItem(request.getCid(), request.getSort(), request.getSize(), request.getPage(), request.getPriceGt(),request.getPriceLte());
            Long integer = itemMapper.selectItemCount(request.getCid(), request.getPriceGt(), request.getPriceLte());
            //进行数据转换
            List<ProductDto> productDtos = productConverter.items2Dto(items);

            allProductResponse = new AllProductResponse(productDtos, integer);
            allProductResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            allProductResponse.setMsg(ShoppingRetCode.SUCCESS.getMessage());
        } catch (Exception e) {
            log.info("IProductServiceImplZhou#getAllProduct " + e);
            allProductResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            allProductResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        return allProductResponse;
    }

    //推荐商品
    @Override
    public RecommendResponse getRecommendGoods() {

        //直接从constant.GlobalConstants 中获取推荐商品的板块id
        RecommendResponse recommendResponse = new RecommendResponse();
//        PanelContentItemDto

        try{
            List<PanelContentItem> panelContentItems
                    = panelContentMapper.selectPanelContentAndProductWithPanelId(GlobalConstants.RECOMMEND_PANEL_ID);
            Example example = new Example(Panel.class);
            example.and().andEqualTo("id", GlobalConstants.RECOMMEND_PANEL_ID);
            Panel panel = panelMapper.selectOneByExample(example);
            PanelDto panelDto = contentConverter.panen2Dto(panel);
            List<PanelContentItemDto> panelContentItemDtos = contentConverter.panelContentItem2Dto(panelContentItems);
            panelDto.setPanelContentItems(panelContentItemDtos);
            HashSet<PanelDto> panelDtos = new HashSet<>();
            panelDtos.add(panelDto);
            recommendResponse.setPanelContentItemDtos(panelDtos);
            recommendResponse.setCode(ShoppingRetCode.SUCCESS.getCode());
            recommendResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }
        catch(Exception e){
            log.info("IProductServiceImplZhou#getRecommendGoods " + e);
            recommendResponse.setCode(ShoppingRetCode.DB_EXCEPTION.getCode());
            recommendResponse.setMsg(ShoppingRetCode.DB_EXCEPTION.getMessage());
        }

        return recommendResponse;
    }
}
