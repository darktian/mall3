package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import lombok.Data;

import java.math.BigDecimal;

/**
 *  ciggar
 * create-date: 2019/7/24-16:29
 */
@Data
public class AllProductRequestZhou extends AbstractRequest {

    private Integer page;
    private Integer size;
    private Integer sort;
    private Long cid;
    private BigDecimal priceGt;
    private BigDecimal priceLte;

    @Override
    public void requestCheck() {
        if(page == null || page<=0){
            setPage(1);
        }
        BigDecimal bigDecimal = new BigDecimal(0);
        //输入的最大值和最小值不合法
        if(priceGt!= null && bigDecimal.compareTo(priceGt)  > 0){
            priceGt = bigDecimal;
        }
        if(priceLte != null && bigDecimal.compareTo(priceLte)  > 0){
            priceLte = bigDecimal;
        }
        //最大最小值相反
        if(priceLte != null && priceGt != null){
            BigDecimal min = priceLte.min(priceGt);
            BigDecimal max = priceGt.max(priceLte);
            priceLte = max;
            priceGt = min;
        }
    }
}
