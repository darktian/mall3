package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *  ciggar
 * create-date: 2019/7/24-16:29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AllProductResponse extends AbstractResponse {

    private List<ProductDto> productDtoList;

    private Long total;
}
