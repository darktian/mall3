package com.mall.shopping;

import com.mall.shopping.dto.TestProductDetailDto;
import com.mall.shopping.dto.TestProductDetailRequest;
import com.mall.shopping.dto.TestProductDetailResponse;

public interface ITestProductService {

    /*
           获取商品的名称，价格，商品图片的url
     */
    TestProductDetailResponse getProductDetail(TestProductDetailRequest productDetailRequest);
}
