package com.mall.shopping.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/21/10:58
 */
@Data
public class ItemCatDto1 implements Serializable {
    private String iconUrl;
    private Integer id;
    private boolean isParent;
    private String name;
    private Integer parentId;


    public boolean getIsParent() {
		return isParent;
	}

}
