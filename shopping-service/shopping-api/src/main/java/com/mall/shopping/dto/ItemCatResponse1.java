package com.mall.shopping.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;

import java.util.List;

/**
 * @Description:
 * @Author: Tian
 * @Date: 2021/07/21/10:58
 */
@Data
public class ItemCatResponse1 extends AbstractResponse {
    List<ItemCatDto1> itemCatDto1s;
}
