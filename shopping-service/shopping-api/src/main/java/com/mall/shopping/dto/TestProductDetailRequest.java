package com.mall.shopping.dto;

import com.mall.commons.result.AbstractRequest;
import lombok.Data;

@Data
public class TestProductDetailRequest extends AbstractRequest {

    Long productId;


    @Override
    public void requestCheck() {
       if (productId == null || productId < 0) {
           // 抛出异常
       }
    }
}
