package com.mall.user.services;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.druid.sql.visitor.functions.If;
import com.mall.user.IUserLoginService;
import com.mall.user.constants.SysRetCodeConstants;
import com.mall.user.dal.dao.MemberDao;
import com.mall.user.dal.dao.UserVerifyDao;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.entitys.UserVerify;
import com.mall.user.dto.*;
import com.mall.user.utils.JwtTokenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import sun.security.provider.MD5;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
 *@Author: WUW
 *@Description
 *
 */
@Slf4j
@Component
@Service
@Transactional
public class UserLoginServiceImpl implements IUserLoginService {

    @Autowired
    RedissonClient redissonClient;

    @Autowired
    MemberDao memberDao;

    @Autowired
    UserVerifyDao userVerifyDao;

    @Autowired
    private JavaMailSender mailSender;


    @Override
    public CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest) {
        String token = checkAuthRequest.getToken();
        CheckAuthResponse checkAuthResponse = new CheckAuthResponse();
        try {
            checkAuthResponse.setUserinfo(JwtTokenUtils.builder().token(token).build().freeJwt());
        } catch (Exception e) {
            checkAuthResponse.setCode(SysRetCodeConstants.TOKEN_VALID_FAILED.getCode());
            checkAuthResponse.setMsg(SysRetCodeConstants.TOKEN_VALID_FAILED.getMessage());
            return checkAuthResponse;
        }
        checkAuthResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        checkAuthResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return checkAuthResponse;
    }


    @Override
    public UserLoginResponse login(UserLoginRequest userLoginRequest) {
        UserLoginResponse loginResponse = new UserLoginResponse();
        try {
            if (userLoginRequest.getUserName() != null && userLoginRequest.getUserName().endsWith(".com")) {
                Member member2 = new Member();
                member2.setEmail(userLoginRequest.getUserName());
                member2.setIsVerified("Y");
                member2.setPassword(SecureUtil.md5(userLoginRequest.getPassword()));
                Member member1 = memberDao.selectOne(member2);
                System.out.println("eeeeeeeeeeeeeeee " + ObjectUtils.isEmpty(member1));
                if (!ObjectUtils.isEmpty(member1)) {
                    BeanUtils.copyProperties(member1, loginResponse);
                    loginResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
                    loginResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
                    return loginResponse;
                }
            }
            Member member = new Member();
            member.setPassword(SecureUtil.md5(userLoginRequest.getPassword()));
            member.setUsername(userLoginRequest.getUserName());
            Member member1 = memberDao.selectOne(member);
            if(member1.getIsVerified().equals("N")){
                loginResponse.setCode(SysRetCodeConstants.USER_ISVERFIED_ERROR.getCode());
                loginResponse.setMsg(SysRetCodeConstants.USER_ISVERFIED_ERROR.getMessage());
                return loginResponse;
            }
            if (ObjectUtils.isEmpty(member1)) {
                loginResponse.setCode(SysRetCodeConstants.USERORPASSWORD_ERRROR.getCode());
                loginResponse.setMsg(SysRetCodeConstants.USERORPASSWORD_ERRROR.getMessage());
                return loginResponse;
            }
            BeanUtils.copyProperties(member1, loginResponse);
        } catch (Exception e) {
            e.printStackTrace();
            loginResponse.setCode(SysRetCodeConstants.DB_EXCEPTION.getCode());
            loginResponse.setMsg(SysRetCodeConstants.DB_EXCEPTION.getMessage());
            return loginResponse;
        }
        loginResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        loginResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return loginResponse;
    }

    @Override
    public String getTokens(String username) {
        return JwtTokenUtils.builder().msg(username).build().creatJwtToken();
    }

    @Override
    public String getUserInfoById(Long userId) {
        Member member = memberDao.selectByPrimaryKey(userId);
        return member.getUsername();
    }

    @Override
    public MemberResponse getUserInfo(String userToken) {
        MemberResponse memberResponse = new MemberResponse();
        try {
            Example example = new Example(Member.class);
            example.createCriteria().andEqualTo("username", userToken);
            Member members = memberDao.selectOneByExample(example);
            memberResponse.setFile(members.getFile());
            memberResponse.setUid(members.getId());
            memberResponse.setUsername(members.getUsername());
        } catch (Exception e) {
            e.printStackTrace();
            memberResponse.setCode(SysRetCodeConstants.DB_EXCEPTION.getCode());
            memberResponse.setMsg(SysRetCodeConstants.DB_EXCEPTION.getMessage());
            return memberResponse;
        }
        memberResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        memberResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return memberResponse;
    }


    @Override
    public UserRegisterResponse register(UserRegisterRequest userRegisterRequest) {
        UserRegisterResponse userRegisterResponse = new UserRegisterResponse();
        try {
            Member member = new Member();
            member.setUsername(userRegisterRequest.getUserName());
            Member member1 = memberDao.selectOne(member);
            if (!ObjectUtils.isEmpty(member1)) {
                userRegisterResponse.setCode(SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getCode());
                userRegisterResponse.setMsg(SysRetCodeConstants.USERNAME_ALREADY_EXISTS.getMessage());
                return userRegisterResponse;
            }
            UserVerify userVerify = new UserVerify();
            BeanUtils.copyProperties(userRegisterRequest, member);
            member.setPassword(SecureUtil.md5(userRegisterRequest.getUserPwd()));
            member.setCreated(new Date());
            member.setUpdated(new Date());
            member.setIsVerified("N");
            member.setState(1);
            memberDao.insert(member);
            String s1 = IdUtil.fastSimpleUUID();
            new Thread(()->{
                System.out.println("开始发送邮件");
                sendMail(member.getEmail(), member.getUsername(), s1);

                System.out.println("邮件发送成功");
            }).start();
            userVerify.setUuid(s1);
            userVerify.setUsername(member.getUsername());
            userVerify.setRegisterDate(new Date());
            userVerify.setIsExpire("N");
            userVerify.setIsVerify("N");
            userVerifyDao.insertSelective(userVerify);
        } catch (BeansException e) {
            e.printStackTrace();
            userRegisterResponse.setCode(SysRetCodeConstants.DB_EXCEPTION.getCode());
            userRegisterResponse.setMsg(SysRetCodeConstants.DB_EXCEPTION.getMessage());
            return userRegisterResponse;
        }
        userRegisterResponse.setCode(SysRetCodeConstants.SUCCESS.getCode());
        userRegisterResponse.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
        return userRegisterResponse;
    }

    private void sendMail(String email, String username, String s1) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("mall激活");
        message.setText("http://localhost:8080/user/verify?userName=" + username + "&uuid=" + s1);
        message.setTo(email);
        message.setFrom("2684278586@qq.com");
        mailSender.send(message);
    }

    @Override
    public UserVerifyResponse verifyActive(UserVerifyRequest userVerifyRequest) {
        UserVerifyResponse response = new UserVerifyResponse();
        try {
            UserVerify verify = new UserVerify();
            verify.setUsername(userVerifyRequest.getUserName());
            UserVerify userVerify1 = userVerifyDao.selectOne(verify);
            if (ObjectUtils.isEmpty(userVerify1)) {
                response.setCode(SysRetCodeConstants.USER_INFOR_INVALID.getCode());
                response.setMsg(SysRetCodeConstants.USER_INFOR_INVALID.getMessage());
                return response;
            }
            String code = userVerify1.getUuid();
            if (StringUtils.isNotBlank(code) && userVerifyRequest.getUuid().equals(code)) {
                System.out.println("ss");
                UserVerify userVerify = new UserVerify();
                userVerify.setIsVerify("Y");
                Example example = new Example(UserVerify.class);
                example.createCriteria().andEqualTo("username", userVerifyRequest.getUserName());
                userVerifyDao.updateByExampleSelective(userVerify, example);
                Member member = new Member();
                Example exampleMember = new Example(Member.class);
                exampleMember.createCriteria().andEqualTo("username", userVerifyRequest.getUserName());
                member.setIsVerified("Y");
                memberDao.updateByExampleSelective(member, exampleMember);
                response.setCode(SysRetCodeConstants.SUCCESS.getCode());
                response.setMsg(SysRetCodeConstants.SUCCESS.getMessage());
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCode(SysRetCodeConstants.SYSTEM_ERROR.getCode());
            response.setMsg(SysRetCodeConstants.SYSTEM_ERROR.getMessage());
            return response;
        }
        response.setCode(SysRetCodeConstants.USER_REGISTER_VERIFY_FAILED.getCode());
        response.setMsg(SysRetCodeConstants.USER_REGISTER_VERIFY_FAILED.getMessage());
        return response;
    }
}