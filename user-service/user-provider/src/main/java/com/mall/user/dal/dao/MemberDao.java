package com.mall.user.dal.dao;

import com.mall.user.dal.entitys.Member;
import tk.mybatis.mapper.common.Mapper;

/*
 *@Author: WUW
 *@Description
 *
 */
public interface MemberDao extends Mapper<Member> {
}
