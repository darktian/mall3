package com.mall.user.bootstrap;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

/**
 * @author: jia.xue
 * @Email: xuejia@cskaoyan.onaliyun.com
 * @Description
 **/
public class MailTest extends UserProviderApplicationTests{

    @Autowired
    private JavaMailSender mailSender;

    @Test
    public void sendEmail(){

        SimpleMailMessage message = new SimpleMailMessage();

        message.setSubject("csmall激活");
        message.setText("ddd");
        message.setTo("18270092927@163.com");
        message.setFrom("2684278586@qq.com");

        mailSender.send(message);

        System.out.println("---------发送邮件成功----------");

    }
}