package com.mall.user.dto;

import com.mall.commons.result.AbstractRequest;
import lombok.Data;

/*
 *@Author: WUW
 *@Description
 *
 */
@Data
public class MemberRequest extends AbstractRequest {
    private String userName;
    private String password;
    private String captcha;


    @Override
    public void requestCheck() {

    }
}
