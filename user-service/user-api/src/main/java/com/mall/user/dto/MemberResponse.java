package com.mall.user.dto;

import com.mall.commons.result.AbstractResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/*
 *@Author: WUW
 *@Description
 *
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MemberResponse extends AbstractResponse {
    private String file;
    private Long uid;
    private String username;

}
