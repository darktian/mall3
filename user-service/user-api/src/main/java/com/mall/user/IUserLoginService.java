package com.mall.user;

import com.mall.user.dto.*;

/*
 *@Author: WUW
 *@Description
 *
 */

public interface IUserLoginService {


    CheckAuthResponse validToken(CheckAuthRequest checkAuthRequest);

    /**
     * 登录
     * @MethodName login
     * @Params [userLoginRequest]
     * @return com.mall.user.dto.UserLoginResponse
     */
    UserLoginResponse login(UserLoginRequest userLoginRequest);

    String getTokens(String username);

    /**
     * 获取用户信息
     * @MethodName getUserInfo
     * @Params [userToken]
     * @return com.mall.user.dto.MemberResponse
     */
    MemberResponse getUserInfo(String userToken);

    /**
     *
     * @param userId
     * @return
     */
    String getUserInfoById(Long userId);


    /**
     * 用户注册
     * @MethodName regsiter
     * @Params [userRegisterRequest]
     * @return com.mall.user.dto.UserRegisterResponse
     */
    UserRegisterResponse register(UserRegisterRequest userRegisterRequest);


    /**
     * 邮件激活
     * @MethodName verifyActive
     * @Params [userVerifyRequest]
     * @return com.mall.user.dto.UserVerifyResponse
     */
    UserVerifyResponse verifyActive(UserVerifyRequest userVerifyRequest);
}
